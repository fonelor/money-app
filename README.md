# README #

Simple application to demonstrate money transfer from one account/user to another

### Money-app ###

* 0.1

### Build ###

* checkout with git
* build with maven
* configured to use in-memory h2 db
* testing: ``` maven test ```
* jar: ``` maven jar ```
* execute: ``` java -jar {built-jar} ```
* execute with own config: ``` java -jar {built-jar} server {myconfig.yml} ```

### Example config ###
```
#!yaml

server:
  applicationConnectors:
    - type: http
      port: 8090
  adminConnectors:
    - type: http
      port: 8091

# Logging settings.
logging:

  # The default level of all loggers. Can be OFF, ERROR, WARN, INFO, DEBUG, TRACE, or ALL.
  level: INFO

  # Logger-specific levels.
  loggers:

    # Sets the level for 'ru.rolsoft.moneyapp' to DEBUG.
    ru.rolsoft.moneyapp: DEBUG

    # Redirects SQL logs to a separate file
    com.j256.ormlite: WARN

# data base url
dburl: "jdbc:h2:mem:moneyapp"

```