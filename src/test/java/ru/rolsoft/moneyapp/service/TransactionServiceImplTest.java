package ru.rolsoft.moneyapp.service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.TableUtils;
import org.junit.Before;
import org.junit.Test;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.data.Transaction;
import ru.rolsoft.moneyapp.data.TransactionType;
import ru.rolsoft.moneyapp.data.User;

import javax.transaction.InvalidTransactionException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.time.Instant;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Sergey Filippov on 12.04.2017.
 */
public class TransactionServiceImplTest {

    private Dao<Transaction, Long> transactionDao;

    private Dao<Account, Long> accountDao;

    private TransactionServiceImpl transactionService;

    private User user;

    private Account account;

    private Account account2;

    private UpdateBuilder updateBuilder;

    private Where where;

    private DatabaseConnection databaseConnection;

    private QueryBuilder queryBuilder;

    @Before
    public void setUp()
            throws Exception {
        transactionDao = mock(Dao.class);
        accountDao = mock(Dao.class);

        transactionService = new TransactionServiceImpl(accountDao, transactionDao);
        user = new User("user");
        account = new Account(user, "Account1", BigDecimal.TEN, false);
        account.setId(1L);
        account2 = new Account(user, "Account2", BigDecimal.ZERO, false);
        account2.setId(2L);

        databaseConnection = mock(DatabaseConnection.class);

        updateBuilder = mock(UpdateBuilder.class);
        queryBuilder = mock(QueryBuilder.class);

        where = mock(Where.class);

        when(accountDao.updateBuilder()).thenReturn(updateBuilder);
        when(accountDao.queryBuilder()).thenReturn(queryBuilder);
        when(queryBuilder.where()).thenReturn(where);
        when(updateBuilder.where()).thenReturn(where);
        when(updateBuilder.updateColumnValue(anyString(), any())).thenReturn(updateBuilder);

        when(where.eq(anyString(), any())).thenReturn(where);
        when(where.and()).thenReturn(where);
        when(where.or()).thenReturn(where);
    }

    @Test(expected = InvalidTransactionException.class)
    public void processTransaction_failNoSource()
            throws Exception {

        when(transactionDao.idExists(anyLong())).thenReturn(false);
        when(accountDao.queryForId(account.getId())).thenReturn(null);
        when(accountDao.queryForId(account2.getId())).thenReturn(account2);

        Transaction transaction = new Transaction(account, account2, BigDecimal.ZERO, Date.from(Instant
                .EPOCH));
        transactionService.processTransaction(transaction);

        verifyNoMoreInteractions(transactionDao);
        verifyNoMoreInteractions(accountDao);
    }

    @Test(expected = InvalidTransactionException.class)
    public void processTransaction_failNoDestination()
            throws Exception {

        when(transactionDao.idExists(anyLong())).thenReturn(false);
        when(accountDao.queryForId(account.getId())).thenReturn(account);
        when(accountDao.queryForId(account2.getId())).thenReturn(null);

        Transaction transaction = new Transaction(account, account2, BigDecimal.ZERO, Date.from(Instant
                .EPOCH));
        transactionService.processTransaction(transaction);

        verifyNoMoreInteractions(transactionDao);
        verifyNoMoreInteractions(accountDao);
    }

    @Test(expected = InvalidTransactionException.class)
    public void processTransaction_failTransactionExists()
            throws Exception {

        when(transactionDao.idExists(anyLong())).thenReturn(true);
        when(accountDao.queryForId(account.getId())).thenReturn(account);
        when(accountDao.queryForId(account2.getId())).thenReturn(account2);

        Transaction transaction = new Transaction(account, account2, BigDecimal.ZERO, Date.from(Instant
                .EPOCH));
        transactionService.processTransaction(transaction);
    }

    @Test(expected = InvalidTransactionException.class)
    public void processTransaction_failTransactionAmountGreaterThanSource()
            throws Exception {

        when(transactionDao.idExists(anyLong())).thenReturn(false);
        when(accountDao.queryForId(account.getId())).thenReturn(account);
        when(accountDao.queryForId(account2.getId())).thenReturn(account2);

        Transaction transaction = new Transaction(account, account2, BigDecimal.valueOf(20),
                Date.from(Instant.EPOCH));
        transactionService.processTransaction(transaction);

        verifyNoMoreInteractions(transactionDao);
        verifyNoMoreInteractions(accountDao);
    }

    @Test(expected = InvalidTransactionException.class)
    public void processTransaction_failTransactionTransactionAmountIsZero()
            throws Exception {

        when(transactionDao.idExists(anyLong())).thenReturn(false);
        when(accountDao.queryForId(account.getId())).thenReturn(account);
        when(accountDao.queryForId(account2.getId())).thenReturn(account2);

        Transaction transaction = new Transaction(account, account2, BigDecimal.ZERO, Date.from(Instant
                .EPOCH));
        transactionService.processTransaction(transaction);

        verifyNoMoreInteractions(transactionDao);
        verifyNoMoreInteractions(accountDao);
    }

    @Test
    public void processTransaction_TransactionSuccess()
            throws Exception {

        when(transactionDao.idExists(anyLong())).thenReturn(false);
        when(accountDao.queryForId(account.getId())).thenReturn(account);
        when(accountDao.queryForId(account2.getId())).thenReturn(account2);
        when(accountDao.startThreadConnection()).thenReturn(databaseConnection);

        when(updateBuilder.prepareStatementString()).thenReturn("stmt");

        Transaction transaction = new Transaction(account, account2, BigDecimal.valueOf(5),
                Date.from(Instant.EPOCH));
        transactionService.processTransaction(transaction);

        verify(databaseConnection, times(2)).executeStatement("stmt",
                DatabaseConnection.DEFAULT_RESULT_FLAGS);

        verify(transactionDao, times(1)).create(transaction);
        verify(accountDao, times(1)).endThreadConnection(databaseConnection);

        verify(databaseConnection, times(1)).commit(null);
    }

    @Test(expected = RuntimeException.class)
    public void processTransaction_TransactionRollBackOnError()
            throws Exception {

        when(transactionDao.idExists(anyLong())).thenReturn(false);
        when(accountDao.queryForId(account.getId())).thenReturn(account);
        when(accountDao.queryForId(account2.getId())).thenReturn(account2);
        when(accountDao.startThreadConnection()).thenReturn(databaseConnection);
        when(accountDao.updateBuilder()).thenThrow(new RuntimeException("blob"));

        Transaction transaction = new Transaction(account, account2, BigDecimal.valueOf(5),
                Date.from(Instant.EPOCH));
        transactionService.processTransaction(transaction);

        verify(databaseConnection, never()).executeStatement("stmt",
                DatabaseConnection.DEFAULT_RESULT_FLAGS);

        verify(transactionDao, never()).create(transaction);
        verify(accountDao, never()).endThreadConnection(databaseConnection);

        verify(databaseConnection, never()).commit(null);

        verify(accountDao, times(1)).rollBack(databaseConnection);
        verify(transactionDao, times(1)).rollBack(databaseConnection);
    }

    @Test
    public void lockAccount()
            throws Exception {
        Dao<Account, Long> accountDao = realAccountDao(getJdbcConnectionSource());

        final TransactionServiceImpl transactionService = new TransactionServiceImpl(
                accountDao, transactionDao
        );

        transactionService.lock(1L);

        assertTrue(accountDao.queryForId(1L).getLock());

        assertFalse(transactionService.lock(1L));
    }

    @Test
    public void unlockAccount()
            throws Exception {
        Dao<Account, Long> accountDao = realAccountDao(getJdbcConnectionSource());

        final TransactionServiceImpl transactionService = new TransactionServiceImpl(
                accountDao, transactionDao
        );

        assertFalse(transactionService.unlock(1L));

        transactionService.lock(1L);

        assertTrue(transactionService.unlock(1L));
    }

    @Test
    public void getAllTransactionForAccount()
            throws Exception {

        final JdbcConnectionSource jdbcConnectionSource = getJdbcConnectionSource();
        Dao<Account, Long> accountDao = realAccountDao(jdbcConnectionSource);
        Dao<Transaction, Long> transactionDao = realTransactionDao(jdbcConnectionSource);

        accountDao.create(account2);

        final TransactionServiceImpl transactionService = new TransactionServiceImpl(
                accountDao, transactionDao
        );

        assertThat(transactionService.getTransactionsForAccount(1L, TransactionType.All).size(),
                is(0));

        transactionDao
                .create(new Transaction(account, account2, BigDecimal.TEN, Date.from(Instant.EPOCH)));

        assertThat(transactionService.getTransactionsForAccount(1L, TransactionType.All).size(),
                is(1));

        // only income
        assertThat(transactionService.getTransactionsForAccount(1L, TransactionType.Income).size(),
                is(0));

        // only outcome
        assertThat(transactionService.getTransactionsForAccount(1L, TransactionType.Outcome).size(),
                is(1));
    }

    private Dao<Transaction, Long> realTransactionDao(
            final JdbcConnectionSource jdbcConnectionSource)
            throws SQLException {
        Dao<Transaction, Long> transactionDao = DaoManager.createDao(jdbcConnectionSource,
                Transaction.class);
        TableUtils.dropTable(transactionDao, true);
        TableUtils.createTable(transactionDao);

        return transactionDao;
    }

    private Dao<Account, Long> realAccountDao(final JdbcConnectionSource connectionSource)
            throws SQLException {
        Dao<Account, Long> accountDao =
                DaoManager.createDao(connectionSource, Account.class);
        TableUtils.dropTable(accountDao, true);
        TableUtils.createTable(accountDao);

        accountDao.create(account);
        return accountDao;
    }

    private JdbcConnectionSource getJdbcConnectionSource()
            throws SQLException {
        return new JdbcConnectionSource("jdbc:h2:mem:account");
    }
}