import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import ru.rolsoft.moneyapp.MoneyApp;
import ru.rolsoft.moneyapp.MoneyAppConfiguration;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.data.User;
import ru.rolsoft.moneyapp.dto.TransactionDto;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Transfer test shows main use case - transfer money from one account to another
 * Created by Filippov Sergey on 13.04.2017.
 */
public class TransferTest {
    @ClassRule
    public static final DropwizardAppRule<MoneyAppConfiguration> RULE =
            new DropwizardAppRule<>(MoneyApp.class,
                    ResourceHelpers.resourceFilePath("application.yml"));

    private static Client client;

    @BeforeClass
    public static void setUp() {
        client =
                new JerseyClientBuilder(RULE.getEnvironment()).build("test-client");
    }

    @Test
    public void transfer() {
        final String userName = "TestUser";
        final String account1Name = "Account-1";
        final String account2Name = "Account-2";

        // first create user
        final User user = createUser(userName);

        // create 1st account
        Account account1 = createAccount(user, account1Name);

        // create 2nd account
        Account account2 = createAccount(user, account2Name);

        // add money to 1st account
        account1 = addMoneyToAccount(account1, BigDecimal.TEN);

        // transfer money from 1st account to 2nd
        final BigDecimal sum = new BigDecimal(5);
        transferMoney(account1, account2, sum);

        // check accounts
        Collection<Account> accounts = getUserAccounts(user);
        for (Account a : accounts) {
            assertThat(a.getAmount().compareTo(sum), is(0));
        }
    }

    private Collection<Account> getUserAccounts(final User user) {
        String requestBuilder = "http://localhost:" +
                RULE.getLocalPort() +
                "/api/1.0/account/getAccountsByUserId?userId=" +
                user.getId();

        final Response response = client.target(requestBuilder)
                .request()
                .get();

        assertThat(response.getStatus(), is(200));
        final Collection<Account> accounts = response
                .readEntity(new GenericType<Collection<Account>>() {
                });

        assertThat(accounts.size(), is(2));

        return accounts;
    }

    private void transferMoney(final Account sourceAccount, final Account destinationAccount,
                               final BigDecimal amount) {
        String requestBuilder = "http://localhost:" +
                RULE.getLocalPort() +
                "/api/1.0/account/transfer?fromUser=" +
                sourceAccount.getOwner().getId() +
                "&sourceAccount=" +
                sourceAccount.getId() +
                "&toUser=" +
                destinationAccount.getOwner().getId() +
                "&destinationAccount=" +
                destinationAccount.getId() +
                "&amount=" +
                amount;

        final Response response = client.target(requestBuilder)
                .request()
                .get();

        assertThat(response.getStatus(), is(200));
        final TransactionDto transaction = response.readEntity(TransactionDto.class);
        assertThat(transaction.getAmount().compareTo(amount), is(0));
    }

    private Account addMoneyToAccount(final Account account, final BigDecimal amount) {
        String requestBuilder = "http://localhost:" +
                RULE.getLocalPort() +
                "/api/1.0/account/" +
                account.getId() +
                "/addMoney?userId=" +
                account.getOwner().getId() +
                "&amount=" +
                amount;

        final Response response = client.target(requestBuilder)
                .request()
                .get();

        assertThat(response.getStatus(), is(200));
        final Account updatedAccount = response.readEntity(Account.class);
        assertThat(updatedAccount.getAmount(), is(account.getAmount().add(amount)));
        return updatedAccount;
    }

    private User createUser(String testUserName) {
        final Response response = client.target(
                String.format("http://localhost:%d/api/1.0/user/create?userName=%s",
                        RULE.getLocalPort(), testUserName))
                .request()
                .get();

        assertThat(response.getStatus(), is(200));
        final User user = response.readEntity(User.class);
        assertThat(user.getId(), not(0));
        assertThat(user.getName(), is(testUserName));

        return user;
    }

    private Account createAccount(User user, String testAccountName) {
        String requestBuilder = "http://localhost:" +
                RULE.getLocalPort() +
                "/api/1.0/account/create?userId=" +
                user.getId() +
                "&accountName=" +
                testAccountName;

        final Response response = client.target(requestBuilder)
                .request()
                .get();

        assertThat(response.getStatus(), is(200));
        final Account account = response.readEntity(Account.class);
        assertThat(account.getId(), not(0));
        assertThat(account.getOwner(), is(user));
        assertThat(account.getName(), is(testAccountName));
        assertThat(account.getAmount(), is(BigDecimal.ZERO));
        assertThat(account.getLock(), is(false));

        return account;
    }
}
