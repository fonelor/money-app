package ru.rolsoft.moneyapp;

import com.netflix.governator.guice.LifecycleInjector;
import io.dropwizard.Application;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.extern.slf4j.Slf4j;
import ru.vyarus.dropwizard.guice.GuiceBundle;

@Slf4j
public class MoneyApp extends Application<MoneyAppConfiguration> {

    private static boolean configFromJar = false;

    public static void main(String[] args)
            throws Exception {
        configFromJar = args.length < 2;
        new MoneyApp().run(configFromJar ? new String[]{"server", "application.yml"} : args);
    }

    @Override
    public void initialize(Bootstrap<MoneyAppConfiguration> bootstrap) {
        log.info("initialize");

        if (configFromJar) {
            bootstrap.setConfigurationSourceProvider(
                    new ResourceConfigurationSourceProvider());
        }

        bootstrap.addBundle(GuiceBundle.builder()
                .enableAutoConfig(getClass().getPackage().getName())
                .modules(new DbModule(), new ServiceModule())
                .injectorFactory((stage, modules) -> LifecycleInjector.builder()
                        .withModules(modules)
                        .build().createInjector())
                .build());
    }

    public void run(final MoneyAppConfiguration configuration, final Environment environment)
            throws Exception {
    }

    @Override
    public String getName() {
        return "money-app";
    }
}
