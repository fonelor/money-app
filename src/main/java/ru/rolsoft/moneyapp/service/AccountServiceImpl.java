package ru.rolsoft.moneyapp.service;

import com.j256.ormlite.dao.Dao;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.data.Transaction;
import ru.rolsoft.moneyapp.data.User;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.InvalidTransactionException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Sergey Filippov on 12.04.17.
 */
public class AccountServiceImpl implements AccountService {

    private final Dao<Account, Long> accountDao;

    private final Dao<User, Long> userDao;

    private final TransactionService transactionService;

    @Inject
    public AccountServiceImpl(@Named("account") Dao<Account, Long> accountDao,
                              @Named("user") final Dao<User, Long> userDao,
                              final TransactionService transactionService) {
        this.accountDao = accountDao;
        this.userDao = userDao;
        this.transactionService = transactionService;
    }

    @Override
    public Account addMoney(final Long userId, final Long accountId, final BigDecimal amount)
            throws SQLException, InvalidTransactionException {
        final Date timestamp = Date.from(Instant.now());
        final Account sourceAccount = create(userId, "income-" + timestamp, true);
        if (sourceAccount != null) {
            sourceAccount.setAmount(amount);
            accountDao.update(sourceAccount);
            final Account destinationAccount = accountDao.queryForId(accountId);
            if (transactionService.processTransaction(
                    new Transaction(sourceAccount, destinationAccount, amount, timestamp))) {
                accountDao.refresh(destinationAccount);
                return destinationAccount;
            }
        }
        return null;
    }

    @Override
    public Collection<Account> getAccountsByUserId(Long userId)
            throws SQLException {
        return accountDao.queryBuilder()
                .where().eq("owner_id", userId)
                .and().eq("service", false)
                .query();
    }

    @Override
    public Account getById(final Long accountId)
            throws SQLException {
        return accountDao.queryForId(accountId);
    }

    @Override
    public Account create(final Long userId, final String name, final Boolean service)
            throws SQLException {
        final User user = userDao.queryForId(userId);
        if (user != null &&
                accountDao.queryBuilder()
                        .where().eq("name", name)
                        .countOf() == 0) {
            final Account account = new Account(user, name, BigDecimal.ZERO, service);
            return accountDao.create(account) == 1 ? account : null;
        } else {
            return null;
        }
    }
}