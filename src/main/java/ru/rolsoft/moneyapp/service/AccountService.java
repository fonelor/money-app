package ru.rolsoft.moneyapp.service;

import org.hibernate.validator.constraints.NotEmpty;
import ru.rolsoft.moneyapp.data.Account;

import javax.transaction.InvalidTransactionException;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by Sergey Filippov on 12.04.17.
 */
public interface AccountService {
    Account addMoney(@NotNull Long userId, @NotNull Long accountId, @NotNull BigDecimal amount)
            throws SQLException, InvalidTransactionException;

    Collection<Account> getAccountsByUserId(@NotNull Long userId)
            throws SQLException;

    Account getById(@NotNull Long accountId)
            throws SQLException;

    Account create(@NotNull Long userId, @NotEmpty String name, Boolean service)
            throws SQLException;
}
