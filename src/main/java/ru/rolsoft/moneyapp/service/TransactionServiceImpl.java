package ru.rolsoft.moneyapp.service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.DatabaseConnection;
import lombok.extern.slf4j.Slf4j;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.data.Transaction;
import ru.rolsoft.moneyapp.data.TransactionType;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.InvalidTransactionException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by Sergey Filippov on 12.04.2017.
 */
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    private final Dao<Account, Long> accountDao;

    private final Dao<Transaction, Long> transactionDao;

    @Inject
    public TransactionServiceImpl(
            @Named("account") final Dao<Account, Long> accountDao,
            @Named("transaction") final Dao<Transaction, Long> transactionDao) {
        this.accountDao = accountDao;
        this.transactionDao = transactionDao;
    }

    @Override
    public boolean processTransaction(final Transaction transaction)
            throws SQLException, InvalidTransactionException {
        if (transaction.getId() == null && !transactionDao.idExists(transaction.getId())) {

            final Account source = accountDao.queryForId(transaction.getSource().getId());
            final Account destination = accountDao.queryForId(transaction.getDestination().getId());

            if (source != null && destination != null) {

                if (lock(source.getId()) && lock(destination.getId())) {

                    final BigDecimal amount = transaction.getAmount();

                    if (amount.compareTo(BigDecimal.ZERO) > 0 &&
                            source.getAmount().compareTo(amount) >= 0) {

                        DatabaseConnection databaseConnection = null;
                        try {
                            databaseConnection = accountDao.startThreadConnection();
                            transactionDao.setAutoCommit(databaseConnection, false);
                            accountDao.setAutoCommit(databaseConnection, false);

                            source.setAmount(source.getAmount().add(amount.negate()));
                            destination.setAmount(destination.getAmount().add(amount));

                            databaseConnection.executeStatement(updateAccountAmount(source),
                                    DatabaseConnection.DEFAULT_RESULT_FLAGS);

                            databaseConnection.executeStatement(updateAccountAmount(destination),
                                    DatabaseConnection.DEFAULT_RESULT_FLAGS);

                            transactionDao.create(transaction);

                            databaseConnection.commit(null);

                            return true;
                        } catch (SQLException sqle) {
                            transactionDao.rollBack(databaseConnection);
                            accountDao.rollBack(databaseConnection);
                            log.error("{}", sqle);
                        } finally {
                            if (databaseConnection != null) {
                                accountDao.endThreadConnection(databaseConnection);

                                unlock(source.getId());
                                unlock(destination.getId());
                            }
                        }
                    } else {
                        throw new InvalidTransactionException("Source account's amount is smaller than "
                                + "transaction's amount");
                    }
                } else {
                    throw new InvalidTransactionException("One of the accounts cannot be locked");
                }
            } else {
                throw new InvalidTransactionException("Source/destination account doesn't exists");
            }
        }

        return false;
    }

    @Override
    public Collection<Transaction> getTransactionsForAccount(final Long accountId,
                                                             final TransactionType type)
            throws SQLException {
        final Where<Transaction, Long> where = transactionDao
                .queryBuilder().where();

        if (type == TransactionType.All || type == TransactionType.Income) {
            where.eq("dest_id", accountId);
        }
        if (type == TransactionType.All) {
            where.or();
        }
        if (type == TransactionType.All || type == TransactionType.Outcome) {
            where.eq("source_id", accountId);
        }
        return where.query();
    }

    private String updateAccountAmount(final Account source)
            throws SQLException {
        final UpdateBuilder<Account, Long> updateBuilder = accountDao
                .updateBuilder();

        updateBuilder
                .updateColumnValue("amount", source.getAmount())
                .where().eq("id", source.getId());
        return updateBuilder.prepareStatementString();
    }

    boolean lock(final Long id)
            throws SQLException {
        if (!getLock(id)) {
            try {
                final UpdateBuilder<Account, Long> updateBuilder = accountDao.updateBuilder()
                        .updateColumnValue("lock", true);
                updateBuilder
                        .where().eq("id", id);
                updateBuilder.update();
                return true;
            } catch (SQLException sqle) {
                log.error("Unable to lock account {}: {}", id, sqle);
            }
        }
        return false;
    }

    public boolean unlock(final Long id)
            throws SQLException {
        if (getLock(id)) {
            try {
                final UpdateBuilder<Account, Long> updateBuilder = accountDao.updateBuilder()
                        .updateColumnValue("lock", false);
                updateBuilder
                        .where().eq("id", id);
                updateBuilder.update();
                return true;
            } catch (SQLException sqle) {
                log.error("Unable to lock account {}: {}", id, sqle);
            }
        }
        return false;
    }

    private boolean getLock(final Long id)
            throws SQLException {
        return accountDao.queryBuilder()
                .where().eq("id", id)
                .and().eq("lock", true)
                .countOf() == 1;
    }
}
