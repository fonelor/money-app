package ru.rolsoft.moneyapp.service;

import com.j256.ormlite.dao.Dao;
import ru.rolsoft.moneyapp.data.User;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.SQLException;

/**
 * Created by Sergey Filippov on 13.04.2017.
 */
public class UserServiceImpl implements UserService {

    private final Dao<User, Long> userDao;

    @Inject
    public UserServiceImpl(
            @Named("user") final Dao<User, Long> userDao) {
        this.userDao = userDao;
    }

    @Override
    public User getById(final Long id)
            throws SQLException {
        return userDao.queryForId(id);
    }

    @Override
    public User create(final String userName)
            throws SQLException {
        if (userDao.queryBuilder()
                .where().eq("name", userName)
                .countOf() == 0) {
            User newUser = new User(userName);
            return userDao.create(newUser) == 1 ? newUser : null;
        } else {
            return null;
        }
    }
}
