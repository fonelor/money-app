package ru.rolsoft.moneyapp.service;

import ru.rolsoft.moneyapp.data.Transaction;
import ru.rolsoft.moneyapp.data.TransactionType;

import javax.transaction.InvalidTransactionException;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by Sergey Filippov on 12.04.2017.
 */
public interface TransactionService {
    boolean processTransaction(Transaction transaction)
            throws SQLException, InvalidTransactionException;

    Collection<Transaction> getTransactionsForAccount(Long accountId,
                                                      final TransactionType income)
            throws SQLException;
}
