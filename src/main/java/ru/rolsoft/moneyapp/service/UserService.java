package ru.rolsoft.moneyapp.service;

import ru.rolsoft.moneyapp.data.User;

import javax.validation.constraints.NotNull;
import java.sql.SQLException;

/**
 * Created by Sergey Filippov on 13.04.2017.
 */
public interface UserService {
    User getById(@NotNull Long id)
            throws SQLException;

    User create(@NotNull String userName)
            throws SQLException;
}
