package ru.rolsoft.moneyapp.api;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

abstract class AbstractApi {

    private static final Response BAD = Response.status(Status.BAD_REQUEST).entity("{}").build();
    private static final Response SERVER_ERROR = Response.serverError().entity("{}").build();

    Response buildOkResponse(Object body) {
        return Response.ok().entity(body).build();
    }

    Response badRequest() {
        return BAD;
    }

    Response serverError() {
        return SERVER_ERROR;
    }
}
