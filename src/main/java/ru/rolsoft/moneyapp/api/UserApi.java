package ru.rolsoft.moneyapp.api;

import org.hibernate.validator.constraints.NotEmpty;
import ru.rolsoft.moneyapp.data.User;
import ru.rolsoft.moneyapp.service.UserService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

@Path("/api/1.0/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserApi extends AbstractApi {

    private final UserService userService;

    @Inject
    public UserApi(UserService userService) {
        this.userService = userService;
    }

    @GET
    @Path("{userId}")
    public Response getUserById(@PathParam("userId") @NotNull Long userId)
            throws SQLException {
        final User user = userService.getById(userId);
        if (user != null) {
            return buildOkResponse(user);
        } else {
            return badRequest();
        }
    }

    @GET
    @Path("create")
    public Response create(@QueryParam("userName") @NotEmpty String userName)
            throws SQLException {
        User newUser = userService.create(userName);
        if (newUser != null) {
            return buildOkResponse(newUser);
        } else {
            return badRequest();
        }
    }
}
