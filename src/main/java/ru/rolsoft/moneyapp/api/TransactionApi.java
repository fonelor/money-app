package ru.rolsoft.moneyapp.api;

import lombok.extern.slf4j.Slf4j;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.data.Transaction;
import ru.rolsoft.moneyapp.data.TransactionType;
import ru.rolsoft.moneyapp.dto.TransactionDto;
import ru.rolsoft.moneyapp.service.AccountService;
import ru.rolsoft.moneyapp.service.TransactionService;

import javax.inject.Inject;
import javax.transaction.InvalidTransactionException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * Created by Sergey Filippov on 12.04.17.
 */
@Slf4j
@Path("/api/1.0/account")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionApi extends AbstractApi {

    private final TransactionService transactionService;

    private final AccountService accountService;

    @Inject
    public TransactionApi(final TransactionService transactionService,
                          final AccountService accountService) {
        this.transactionService = transactionService;
        this.accountService = accountService;
    }

    @GET
    @Path("transfer")
    public Response transfer(@QueryParam("fromUser") @NotNull Long fromUserId,
                             @QueryParam("sourceAccount") @NotNull Long sourceAccount,
                             @QueryParam("toUser") @NotNull Long toUserId,
                             @QueryParam("destinationAccount") @NotNull Long destinationAccount,
                             @QueryParam("amount") @NotNull Double amount) {
        try {
            Optional<Account> source = accountService.getAccountsByUserId(fromUserId).stream()
                    .filter(account -> Objects.equals(account.getId(), sourceAccount))
                    .findFirst();

            Optional<Account> destination = accountService.getAccountsByUserId(toUserId).stream()
                    .filter(account -> Objects.equals(account.getId(), destinationAccount))
                    .findFirst();

            if (source.isPresent() && destination.isPresent()) {
                Transaction transaction = new Transaction(source.get(), destination.get(),
                        BigDecimal.valueOf(amount), Date.from(Instant.now()));
                if (transactionService.processTransaction(transaction)) {
                    return buildOkResponse(new TransactionDto(transaction));
                } else {
                    return serverError();
                }
            } else {
                return badRequest();
            }
        } catch (SQLException sqle) {
            log.error("Unable to transfer {}", sqle);
            return serverError();
        } catch (InvalidTransactionException e) {
            log.error("Transaction invalid {}", e);
            return badRequest();
        }
    }

    @GET
    @Path("{accountId}/allTransactions")
    public Response allTransactions(@PathParam("accountId") @NotNull Long accountId) {
        return getTransactionsDto(accountId, TransactionType.All);
    }

    @GET
    @Path("{accountId}/incomeTransactions")
    public Response incomeTransactions(@PathParam("accountId") @NotNull Long accountId) {
        return getTransactionsDto(accountId, TransactionType.Income);
    }

    @GET
    @Path("{accountId}/outcomeTransactions")
    public Response outcomeTransactions(@PathParam("accountId") @NotNull Long accountId) {
        return getTransactionsDto(accountId, TransactionType.Outcome);
    }

    private Response getTransactionsDto(final Long accountId, TransactionType type) {
        try {
            final Collection<TransactionDto> transactionsForAccount = transactionService
                    .getTransactionsForAccount(accountId, type)
                    .stream()
                    .map(TransactionDto::new)
                    .collect(toList());
            return buildOkResponse(transactionsForAccount);
        } catch (SQLException e) {
            log.error("{}", e);
            return serverError();
        }
    }
}
