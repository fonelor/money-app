package ru.rolsoft.moneyapp.api;

import org.hibernate.validator.constraints.NotEmpty;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.service.AccountService;

import javax.inject.Inject;
import javax.transaction.InvalidTransactionException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collection;

@Path("/api/1.0/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountApi extends AbstractApi {

    private final AccountService accountService;

    @Inject
    public AccountApi(AccountService accountService) {
        this.accountService = accountService;
    }

    @GET
    @Path("{accountId}")
    public Response getAccountById(@PathParam("accountId") @NotNull Long accountId)
            throws SQLException {
        final Account account = accountService.getById(accountId);
        if (account != null) {
            return buildOkResponse(account);
        } else {
            return badRequest();
        }
    }

    @GET
    @Path("create")
    public Response create(@QueryParam("userId") @NotNull Long userId,
                           @QueryParam("accountName") @NotEmpty String accountName)
            throws SQLException {
        Account account = accountService.create(userId, accountName, false);
        if (account != null) {
            return buildOkResponse(account);
        } else {
            return badRequest();
        }
    }

    @GET
    @Path("getAccountsByUserId")
    public Collection<Account> getAccountsByUserId(@QueryParam("userId") @NotNull Long userId)
            throws SQLException {
        return accountService.getAccountsByUserId(userId);
    }

    @GET
    @Path("{accountId}/addMoney")
    public Response addMoney(@QueryParam("userId") Long userId,
                             @PathParam("accountId") @NotNull Long accountId,
                             @QueryParam("amount") @NotNull BigDecimal amount)
            throws InvalidTransactionException, SQLException {
        Account account = accountService.addMoney(userId, accountId, amount);
        if (account != null) {
            return buildOkResponse(account);
        } else {
            return badRequest();
        }
    }
}
