package ru.rolsoft.moneyapp;

import com.google.inject.AbstractModule;
import ru.rolsoft.moneyapp.service.*;

import javax.inject.Singleton;

public class ServiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(TransactionService.class).to(TransactionServiceImpl.class).in(Singleton.class);
        bind(AccountService.class).to(AccountServiceImpl.class).in(Singleton.class);
        bind(UserService.class).to(UserServiceImpl.class).in(Singleton.class);
    }
}
