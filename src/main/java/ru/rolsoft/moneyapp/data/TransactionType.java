package ru.rolsoft.moneyapp.data;

/**
 * Created by Sergey Filippov on 13.04.2017.
 */
public enum TransactionType {
    Income,
    Outcome,
    All
}
