package ru.rolsoft.moneyapp.data;

import com.j256.ormlite.field.DatabaseField;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Sergey Filippov on 08.04.2017.
 */
@NoArgsConstructor
@Data
@Entity(name = "accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "owner_id")
    private User owner;

    @Column(name = "name")
    private String name;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "lock")
    private Boolean lock;

    @Column(name = "service")
    private Boolean service;

    public Account(final User owner, String name, final BigDecimal amount, final Boolean service) {
        this.owner = owner;
        this.name = name;
        this.amount = amount;
        this.lock = false;
        this.service = service;
    }
}
