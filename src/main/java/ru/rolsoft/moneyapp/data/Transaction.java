package ru.rolsoft.moneyapp.data;

import com.j256.ormlite.field.DatabaseField;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Sergey Filippov on 08.04.2017.
 */
@NoArgsConstructor
@Data
@Entity(name = "transactions")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @DatabaseField(foreign = true, columnName = "source_id")
    private Account source;

    @DatabaseField(foreign = true, columnName = "dest_id")
    private Account destination;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "timestamp")
    private Date timestamp;

    public Transaction(@NotNull final Account source,
                       @NotNull final Account destination,
                       final BigDecimal amount, final Date timestamp) {
        this.source = source;
        this.destination = destination;
        this.amount = amount;
        this.timestamp = timestamp;
    }
}
