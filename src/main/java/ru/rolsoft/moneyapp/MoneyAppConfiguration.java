package ru.rolsoft.moneyapp;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

public class MoneyAppConfiguration extends Configuration {
    @NotEmpty
    @Getter(onMethod = @__(@JsonProperty))
    @Setter(onMethod = @__(@JsonProperty))
    private String dburl;
}
