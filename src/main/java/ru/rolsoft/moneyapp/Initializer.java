package ru.rolsoft.moneyapp;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;
import io.dropwizard.lifecycle.Managed;
import lombok.extern.slf4j.Slf4j;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.data.Transaction;
import ru.rolsoft.moneyapp.data.User;

import javax.inject.Inject;
import javax.inject.Named;

@Slf4j
public class Initializer implements Managed {

    private final Dao<User, Long> userDao;

    private final Dao<Account, Long> accountDao;

    private final Dao<Transaction, Long> transactionDao;

    @Inject
    public Initializer(
            @Named("user") final Dao<User, Long> userDao,
            @Named("account") final Dao<Account, Long> accountDao,
            @Named("transaction") final Dao<Transaction, Long> transactionDao) {
        this.userDao = userDao;
        this.accountDao = accountDao;
        this.transactionDao = transactionDao;
    }

    @Override
    public void start()
            throws Exception {
        log.info("Started initializer");
        TableUtils.dropTable(userDao, true);
        TableUtils.dropTable(accountDao, true);
        TableUtils.dropTable(transactionDao, true);

        TableUtils.createTable(userDao);
        TableUtils.createTable(accountDao);
        TableUtils.createTable(transactionDao);
    }

    @Override
    public void stop()
            throws Exception {
        log.info("Stopped initializer");
    }
}
