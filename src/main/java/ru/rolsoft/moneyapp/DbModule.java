package ru.rolsoft.moneyapp;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import lombok.extern.slf4j.Slf4j;
import ru.rolsoft.moneyapp.data.Account;
import ru.rolsoft.moneyapp.data.Transaction;
import ru.rolsoft.moneyapp.data.User;

import javax.inject.Singleton;
import java.sql.SQLException;

@Slf4j
public class DbModule extends AbstractModule {

    @Provides
    @Singleton
    @Named("user")
    public Dao<User, Long> provideUserDao(ConnectionSource connectionSource)
            throws SQLException {
        return DaoManager.createDao(connectionSource, User.class);
    }

    @Provides
    @Singleton
    @Named("account")
    public Dao<Account, Long> provideAccountDao(ConnectionSource connectionSource)
            throws SQLException {
        return DaoManager.createDao(connectionSource, Account.class);
    }

    @Provides
    @Singleton
    @Named("transaction")
    public Dao<Transaction, Long> provideTransitionDao(ConnectionSource connectionSource)
            throws SQLException {
        return DaoManager.createDao(connectionSource, Transaction.class);
    }

    @Provides
    @Singleton
    private ConnectionSource createConnectionSource(MoneyAppConfiguration configuration)
            throws SQLException {
        log.info("Creating connection source");
        final JdbcPooledConnectionSource connectionSource =
                new JdbcPooledConnectionSource(configuration.getDburl());
        connectionSource.initialize();
        return connectionSource;
    }

    @Override
    protected void configure() {

    }
}
