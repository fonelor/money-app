package ru.rolsoft.moneyapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rolsoft.moneyapp.data.Transaction;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Sergey Filippov on 13.04.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDto {
    private Long id;

    private Long sourceId;

    private Long destinationId;

    private BigDecimal amount;

    private Date timestamp;

    public TransactionDto(final Transaction transaction) {
        this.id = transaction.getId();
        this.sourceId = transaction.getSource().getId();
        this.destinationId = transaction.getDestination().getId();
        this.amount = transaction.getAmount();
        this.timestamp = transaction.getTimestamp();
    }
}
